LIST=`git diff-index --name-only HEAD | grep -v webide | grep -v eslintrc | grep -E "^[^vendor].*\.*(js|vue)$" | grep -v json`;
if [ "$LIST" ]; then
    eslint $LIST;
fi
