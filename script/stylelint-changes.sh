LIST=`git diff-index --name-only HEAD | grep -v webide | grep -E "^[^vendor].*\.*(scss|css)$" | grep -v json`;
if [ "$LIST" ]; then
    stylelint --config config/stylelint.config.js $LIST;
fi
