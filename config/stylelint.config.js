module.exports = {
  extends: 'stylelint-config-recommended-scss',
  rules: {
    'selector-max-id': 1,
    'selector-max-compound-selectors': 5,
    'no-descending-specificity': [true, { severity: 'warning' }],
    'no-extra-semicolons': [true, { severity: 'error' }],
    'block-no-empty': [true, { severity: 'error' }],
    'block-opening-brace-newline-after': 'always-multi-line',
    'block-closing-brace-newline-before': 'always-multi-line',
    'selector-list-comma-newline-after': 'always-multi-line',
    'declaration-block-semicolon-space-after': 'always-single-line',
    'declaration-block-semicolon-newline-after': 'always',
    'declaration-colon-space-after': 'always'
  }
}
